package csvReader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class AutomataReader {
    public AutomataReader() {

    }

    public ArrayList<String> getAlphabet(String filePath) throws IOException {

        String[] alpha = null;
        ArrayList<String> alphabet = new ArrayList<>();
        BufferedReader csvReader = new BufferedReader(new FileReader(filePath));

        String row = csvReader.readLine();

        if (row != null) {
            alpha = row.split(",");
            for (int i = 0; i < alpha.length; i++) {
                alphabet.add(alpha[i].trim());
            }
        }
        csvReader.close();

        return alphabet;
    }

    public Integer getNumStates(String filePath) throws IOException {

        Integer numStates = 0;

        BufferedReader csvReader = new BufferedReader(new FileReader(filePath));

        csvReader.readLine();

        String row = csvReader.readLine();

        if (row != null) {
            numStates = Integer.valueOf(row);
        }
        csvReader.close();

        return numStates;
    }

    public String[] getFinalStates(String filePath) throws IOException {

        String[] finalStates = null;

        BufferedReader csvReader = new BufferedReader(new FileReader(filePath));

        csvReader.readLine();
        csvReader.readLine();

        String row = csvReader.readLine();

        if (row != null) {
            finalStates = row.split(",");
        }
        csvReader.close();

        return finalStates;
    }

    public ArrayList<String[]> getTransitions(String filePath) throws IOException {

        ArrayList<String[]> transitions = new ArrayList<>();
        String row;

        BufferedReader csvReader = new BufferedReader(new FileReader(filePath));

        csvReader.readLine();
        csvReader.readLine();
        csvReader.readLine();

        while ((row = csvReader.readLine()) != null) {

            transitions.add(row.split(","));

        }
        csvReader.close();

        return transitions;
    }
}