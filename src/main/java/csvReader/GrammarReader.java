package csvReader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class GrammarReader {

    public ArrayList<String[]> getProductions(String filePath) throws IOException {

        ArrayList<String[]> productions = new ArrayList<>();
        String row;

        BufferedReader csvReader = new BufferedReader(new FileReader(filePath));

        while ((row = csvReader.readLine()) != null) {

            productions.add(row.split(" "));

        }
        csvReader.close();

        return productions;
    }
}
