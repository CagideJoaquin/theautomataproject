package main;

import DTO.automata.Automata;
import DTO.automata.State;
import DTO.automata.Transition;

import java.util.ArrayList;
import java.util.Collections;

public class AutomataModel {

    public Automata makeAFD(Automata auto) {

        //Creamos las listas de conjuntos de States.
        ArrayList<ArrayList<State>> states = new ArrayList<>();
        //newStates representa los conjuntos que descubro a medida que analizo el AFND.
        ArrayList<ArrayList<State>> newStates = new ArrayList<>();
        ArrayList<Transition> transitions = new ArrayList<>();

        //Armamos una lista temporal solo para agregar el estado inicial y poder empezar a evaluar nuevos estados
        ArrayList<State> inicial = new ArrayList<>();
        inicial.add(auto.getStates().get(0));
        states.add(inicial);
        newStates.add(inicial);

        //Analizamos los nuevos estados hasta que ya no haya nuevos
        boolean news = true;
        //Variables soporte para cuando el AFND no posee una transición para determinado input.
        boolean trampStateCreated = false;
        State trampState = null;

        while (news) {

            news = false;

            //Lista de conjuntos temporal, lo que guardo aqui seran los nuevos conjuntos que descubra en esta iteración
            ArrayList<ArrayList<State>> temp1 = new ArrayList<>();

            //Para cada conjunto de estados nuevos
            for (ArrayList<State> state : newStates) {
                //Para cada simbolo del alfabeto
                for (String symbol : auto.getAlphabet()) {

                    //Variable temporal donde guardo los states destino que vaya descubriendo en cada iteración por state
                    ArrayList<State> temp2 = new ArrayList<>();

                    for (State s : state) {
                        for (Transition t : auto.getTransitions()) {

                            //Si el estado origen forma parte del conjunto origen y el simbolo es el correcto,
                            //agrego el estado destino a la lista temporal
                            if (t.getOrigen().getName().equals(s.getName()) && t.getSimbolo().equals(symbol)) {
                                if (!contains(temp2, t.getDestino())) {
                                    temp2.add(t.getDestino());
                                }
                            }
                        }
                    }
                    //Si al menos encontre un state destino
                    if (temp2.size() > 0) {
                        //Si no es un conjunto que ya haya encontrado en una iteración previa,
                        //lo agrego a la lista de states y a lista de nuevos
                        if (!containsSet(states, temp2)) {
                            states.add(temp2);
                            temp1.add(temp2);
                            news = true;
                        }

                        transitions.add(new Transition(new State(getName(state), getType(state)), new State(getName(temp2), getType(temp2)), symbol));
                    } else {
                        //Si entro aca significa que el AFND no tiene transiciones hacia ningun estado con ese input.
                        //Creamos un estado trampa donde mandaremos transiciones para inputs que no las tengan
                        if (!trampStateCreated) {
                            trampState = new State("Trampa", "normal");
                            ArrayList<State> trampList = new ArrayList<>();
                            trampList.add(trampState);
                            states.add(trampList);
                            for (String alpha : auto.getAlphabet()) {
                                transitions.add(new Transition(trampState, trampState, alpha));
                            }
                            trampStateCreated = true;
                        }
                        transitions.add(new Transition(new State(getName(state), getType(state)), trampState, symbol));
                        news = true;
                    }
                }
            }
            //Como ya recorrí todos los conjuntos nuevos que tenía,
            //le seteo al conjunto nuevos los que descubri en esta iteración.
            //Si no descubrí ninguno, se termina el while.
            newStates = temp1;
        }
        //Construyo el automata a partir de los nuevos datos.
        return constructAutomata(states, auto.getAlphabet(), transitions);
    }

    private boolean containsSet(ArrayList<ArrayList<State>> states, ArrayList<State> set) {

        for (ArrayList<State> s : states) {
            if (equalSet(s, set))
                return true;
        }
        return false;
    }

    private boolean equalSet(ArrayList<State> s, ArrayList<State> set) {

        if (s == null || set == null) {
            return false;
        }

        if (s.size() != set.size()) {
            return false;
        }

        return s.containsAll(set);
    }

    private String getName(ArrayList<State> states) {

        ArrayList<String> array = new ArrayList<>();

        for (State s : states) {
            array.add(s.getName());
        }

        Collections.sort(array);

        StringBuilder ret = new StringBuilder();

        for (String s : array) {
            ret.append(s);
        }

        return ret.toString();
    }

    private String getType(ArrayList<State> states) {
        for (State s : states) {
            if (s.getType().equals("final")) {
                return "final";
            }
        }
        return "normal";
    }

    private boolean contains(ArrayList<State> states, State s) {
        for (State state : states) {
            if (state.getName().equals(s.getName())) {
                return true;
            }
        }
        return false;
    }

    private Automata constructAutomata(ArrayList<ArrayList<State>> states, ArrayList<String> alphabet, ArrayList<Transition> transitions) {

        ArrayList<State> convertedStates = new ArrayList<>();

        for (ArrayList<State> s : states) {
            State cs = new State(getName(s), getType(s));
            convertedStates.add(cs);
        }

        return new Automata(convertedStates, alphabet, transitions);
    }
}