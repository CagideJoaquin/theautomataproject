package main;

import DTO.parser.Grammar;
import DTO.parser.Production;
import DTO.parser.Symbol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

public class ParserModel {

    private Grammar g;

    public ParserModel(Grammar g) {
        this.g = g;
        this.getFirst();
        this.getFollows();
        this.initializeParsingTable();
        this.fillParsingTable();
    }

    private void getFirst() {

        //Recorro la lista de simbolos. Si es term first(X) = (X)
        //Si no lo es, hago la busqueda en el metodo recursivo.
        for (Symbol s : g.getGrammarSymbols()) {
            if (s.getType().equals("noTerm")) {
                g.addFirst(s, getFirstByNoTerm(s, g));
            } else {
                ArrayList<Symbol> temp = new ArrayList<>();
                temp.add(s);
                g.addFirst(s, temp);
            }
        }
    }

    private ArrayList<Symbol> getFirstByNoTerm(Symbol s, Grammar g) {

        ArrayList<Symbol> firsts = new ArrayList<>();

        for (Production production : g.getProductions()) {

            if (production.getLeft().getName().equals(s.getName())) {
                //Si la primer posición es un term, lo agrego a la lista de first y termino
                if (production.getRight().get(0).getType().equals("term")) {
                    firsts.add(production.getRight().get(0));
                } else {
                    //Si no es terminal: hago la busqueda recursiva.
                    //Siempre entro al menos una vez (i==0).
                    //Si la posición i es nulleable, busco los primeros de i+1.
                    int i = 0;
                    while ((i == 0 || isNulleable(g, production.getRight().get(i - 1))) && i < production.getRight().size()) {
                        firsts.addAll(getFirstByNoTerm(production.getRight().get(i), g));
                        i++;
                    }
                    //Podemos encontrarnos con dos casos. 1_Que la produccion este formada por mas de un simbolo y entramos al if.
                    //2_que sea una producción de un solo simbolo, pero que ese simbolo sea nulleable.
                    if (production.getRight().size() > 1 || production.getRight().size() == 1 && isNulleable(g, production.getRight().get(0))) {
                        //Si la prod tiene mas de 1 simbolo. Si "i" tiene el mismo tamaño que .size(), es porque eran todas nulleables. Agrego /epsilon
                        if (i == production.getRight().size()) {
                            firsts.add(new Symbol("/epsilon", "term"));
                        }
                    }
                }
            }
        }
        return firsts;
    }

    private void getFollows() {

        //Busco los follow de forma recursiva, solo para las variables
        for (Symbol s : g.getGrammarSymbols()) {
            if (s.getType().equals("noTerm")) {
                g.addFollow(s, getFollowByNoTerm(s, g));
            }
        }
    }

    private ArrayList<Symbol> getFollowByNoTerm(Symbol s, Grammar g) {
        ArrayList<Symbol> follows = new ArrayList<>();
        boolean nulleable = false;

        //Si es el simbolo inicial agrego $
        if (s.getName().equals("X_{1}")) {
            follows.add(new Symbol("$", "term"));
        }

        //Recorro las producciones buscando apariciones de s
        for (Production p : g.getProductions()) {
            for (int i = 0; i < p.getRight().size(); i++) {
                //Si encontre a s
                if (p.getRight().get(i).getName().equals(s.getName())) {
                    //Si existe algo del lado derecho de s
                    if (p.getRight().size() - 1 > i) {
                        //Lista temporal donde agrego lo que este a la derecha de s
                        ArrayList<Symbol> temp = new ArrayList<>();
                        for (int j = i + 1; j < p.getRight().size(); j++) {
                            temp.add(p.getRight().get(j));
                        }
                        //Obtengo los first de (/beta...temp)
                        ArrayList<Symbol> first = getFirstForString(temp);
                        //Para cada first encontrado
                        for (Symbol symbol : first) {
                            //Si no es /epsilon lo agrego al follow. Si lo es marco la bandera nulleable.
                            if (!symbol.getName().equals("/epsilon")) {
                                follows.add(symbol);
                            } else {
                                nulleable = true;
                            }
                        }
                    }
                    //Si s no tiene nada del lado derecho o /epsilon pertenecia a first de /beta
                    if (p.getRight().size() - 1 == i || nulleable) {
                        //Esquivamos: Follow(X), prod: X -> aX
                        if (!p.getLeft().getName().equals(s.getName())) {
                            //Llamada recursiva cuando X1 -> aX o
                            //X1 -> aXb con b nulleable
                            follows.addAll(getFollowByNoTerm(p.getLeft(), g));
                        }
                    }
                    nulleable = false;
                }
            }
        }
        return follows;
    }

    private boolean isNulleable(Grammar g, Symbol n) {
        for (Production prod : g.getProductions()) {
            if (prod.getLeft().getName().equals(n.getName())) {
                if (prod.getRight().get(0).getName().equals("/epsilon")) {
                    return true;
                }
            }
        }
        return false;
    }

    private void initializeParsingTable() {
        for (Symbol s : g.getGrammarSymbols()) {
            if (s.getType().equals("noTerm")) {
                g.getTable().put(s, new HashMap<>());
            }
        }
    }

    private void fillParsingTable() {

        ArrayList<Symbol> first;
        ArrayList<Symbol> follow;

        for (Production p : g.getProductions()) {
            //Busco los first(/alpha)
            first = getFirstForString(p.getRight());
            //Para cada symbolo perteneciente a first(/alpha)
            for (Symbol sym : first) {
                //Si es distinto de /epsilon agrego la produccion a la tabla
                if (!sym.getName().equals("/epsilon")) {
                    g.addProduction(p.getLeft(), sym, p);
                } else {
                    //Si no es distinto de epsilon, busco los follow de (X)
                    follow = g.getFollow().get(p.getLeft());
                    //Agrego las producciones a la tabla.
                    //Si entre aca es porque /Epsilon pertenece a First(/Alpha) y si aparece $
                    //es porque pertenece a Follow(X). Por lo tanto no necesito hacer distinción
                    //Para agregar las producciones del estilo Agregar 𝐴→𝛼 a M[A, $]
                    for (Symbol fol : follow) {
                        g.addProduction(p.getLeft(), fol, p);
                    }
                }
            }
        }
    }

    private ArrayList<Symbol> getFirstForString(ArrayList<Symbol> symbols) {

        ArrayList<Symbol> stringFirst = new ArrayList<>();
        ArrayList<Symbol> symbolFirst;
        boolean nulleable = true;
        for (Symbol symbol : symbols) {
            if (nulleable) {
                nulleable = false;
                symbolFirst = g.getFirst().get(symbol);
                for (Symbol s : symbolFirst) {
                    if (!s.getName().equals("/epsilon")) {
                        stringFirst.add(s);
                    } else {
                        nulleable = true;
                    }
                }
            }
        }

        if (nulleable) {
            stringFirst.add(new Symbol("/epsilon", "term"));
        }

        return deleteRepeated(stringFirst);
    }

    private ArrayList<Symbol> deleteRepeated(ArrayList<Symbol> symbols) {
        ArrayList<Symbol> temp = new ArrayList<>();
        for (Symbol sym : symbols) {
            boolean contains = false;
            for (Symbol symbol : temp) {
                if (symbol.getName().equals(sym.getName())) {
                    contains = true;
                }
            }
            if (!contains) {
                temp.add(sym);
            }
        }

        return temp;
    }

    public boolean isPartOfLanguageG(ArrayList<Symbol> w) {
        //Creo la pila
        Stack<Symbol> pila = new Stack<>();
        //La inicializo con $ y simbolo inicial
        pila.push(new Symbol("$", "term"));
        pila.push(g.getGrammarSymbols().get(0));
        Symbol x = pila.peek();
        //Inicializo a con el primer simbolo del input
        int input = 0;
        Symbol a = w.get(input);
        input++;
        Production prodTable;

        while (!x.getName().equals("$")) {
            //Si es un terminal
            if (x.getType().equals("term")) {
                //Si es igual a "a", saco el tope de la lista y a = proximo simbolo
                if (x.getName().equals(a.getName())) {
                    pila.pop();
                    a = w.get(input);
                    input++;
                }
                //Si no es igual a "a" error de parsing.
                else {
                    return false;
                }
            }
            //Si existe una prod en la tabla
            else if ((prodTable = g.getProdFromTable(x, a)) != null) {
                //Saco el tope de la pila y agrego de forma invertida los symbolos de la produccion.
                //Excepto por el /epsilon
                pila.pop();
                for (int i = prodTable.getRight().size(); i >= 1; i--) {
                    if (!prodTable.getRight().get(i - 1).getName().equals("/epsilon")) {
                        pila.push(prodTable.getRight().get(i - 1));
                    }
                }
                System.out.println(prodTable.toString());
            }
            //Si no existe la prod, error de parsing.
            else {
                return false;
            }
            x = pila.peek();
        }
        return true;
    }
}
