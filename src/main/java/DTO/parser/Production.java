package DTO.parser;

import java.util.ArrayList;

public class Production {

    private Symbol left;
    private ArrayList<Symbol> right;

    public Production(Symbol left) {
        this.left = left;
        this.right = new ArrayList<>();
    }

    public void addSymbol(Symbol s) {
        this.right.add(s);
    }

    public Symbol getLeft() {
        return left;
    }

    public ArrayList<Symbol> getRight() {
        return this.right;
    }

    @Override
    public String toString() {
        String ret = left.getName() + " ->";
        for (Symbol s : right) {
            ret = ret + " " + s.getName();
        }
        return ret;
    }

}
