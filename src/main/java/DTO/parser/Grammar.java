package DTO.parser;

import csvReader.GrammarReader;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.*;
import java.util.ArrayList;

public class Grammar {

    private ArrayList<Production> productions;
    private ArrayList<Symbol> grammarSymbols;
    private HashMap<Symbol, ArrayList<Symbol>> first;
    private HashMap<Symbol, ArrayList<Symbol>> follow;
    private HashMap<Symbol, HashMap<Symbol, Production>> table;

    //Constructor de una Gramática a partir de un txt
    public Grammar(String path) {

        this.productions = new ArrayList<>();
        this.grammarSymbols = new ArrayList<>();
        //Inicializo los conjuntos, pero los "cargo" desde el modelo.
        this.first = new HashMap<>();
        this.follow = new HashMap<>();
        this.table = new HashMap<>();

        try {

            GrammarReader r = new GrammarReader();

            //Obtengo las producciones del txt
            ArrayList<String[]> prods = r.getProductions(path);

            for (String[] p : prods) {

                //Reviso el "registry" de symbol. Si ya existe, lo traigo sino, lo creo.
                Symbol var = this.getSymbol(p[0]);
                if (var == null) {
                    var = new Symbol(p[0], "noTerm");
                    this.grammarSymbols.add(var);
                }

                //Creo la producción, solo con el lado izquierdo. Luego agrego el lado derecho
                Production production = new Production(var);

                //i=2 porque salteo las dos primeras posiciones del txt ('variable' '->')
                for (int i = 2; i < p.length; i++) {
                    //Busco el symbolo en el registry
                    Symbol s = this.getSymbol(p[i]);
                    if (s == null) {
                        if (isNonTerm(p[i])) {
                            s = new Symbol(p[i], "noTerm");
                        } else {
                            s = new Symbol(p[i], "term");
                        }
                        this.grammarSymbols.add(s);
                        production.addSymbol(s);
                    } else {
                        production.addSymbol(s);
                    }
                }

                this.productions.add(production);
            }

        } catch (
                Exception e) {
            e.printStackTrace();
        }

    }

    //Agrego los first que corresponden al Mapa, segun el Symbol s
    public void addFirst(Symbol s, ArrayList<Symbol> firsts) {
        //Creo una lista temporal para comprobar que no haya repetidos en el conjuno que viene como parametro
        ArrayList<Symbol> temp = new ArrayList<>();
        for (Symbol sym : firsts) {
            boolean contains = false;
            for (Symbol symbol : temp) {
                if (symbol.getName().equals(sym.getName())) {
                    contains = true;
                }
            }
            if (!contains) {
                temp.add(sym);
            }
        }
        this.getFirst().put(s, temp);

    }

    //Agrego los follow que corresponden al Mapa, segun el Symbol s
    public void addFollow(Symbol s, ArrayList<Symbol> follows) {
        //Creo una lista temporal para comprobar que no haya repetidos en el conjuno que viene como parametro
        ArrayList<Symbol> temp = new ArrayList<>();
        for (Symbol sym : follows) {
            boolean contains = false;
            for (Symbol symbol : temp) {
                if (symbol.getName().equals(sym.getName())) {
                    contains = true;
                }
            }
            if (!contains) {
                temp.add(sym);
            }
        }
        this.getFollow().put(s, temp);

    }

    private Symbol getSymbol(String s) {

        for (Symbol sym : this.grammarSymbols) {
            if (sym.getName().equals(s))
                return sym;
        }
        return null;
    }

    private boolean isNonTerm(String s) {

        return Pattern.matches("X_\\{\\d+}", s);
    }

    public ArrayList<Production> getProductions() {
        return this.productions;
    }

    public ArrayList<Symbol> getGrammarSymbols() {
        return this.grammarSymbols;
    }

    public HashMap<Symbol, ArrayList<Symbol>> getFirst() {
        return first;
    }

    public HashMap<Symbol, ArrayList<Symbol>> getFollow() {
        return follow;
    }

    public HashMap<Symbol, HashMap<Symbol, Production>> getTable() {
        return table;
    }


    //Relleno la tabla de parsing
    public void addProduction(Symbol noTerm, Symbol term, Production p) {
        //Solo voy a rellenar las celdas que corresponde. Si no existe una prod talque M[A,a], entonces no lleno, y devuelvo null.
        //La tabla no sera una matriz exacta de A*a. Solo las celdas tal que M[A,a] tengan una prod.
        for (Map.Entry<Symbol, HashMap<Symbol, Production>> entry : table.entrySet()) {
            if (entry.getKey().getName().equals(noTerm.getName())) {
                this.table.get(entry.getKey()).put(term, p);
            }
        }
    }

    //Devuelvo la producción correspondiente a M[A,a]. Si no corresponde, devuelvo null.
    public Production getProdFromTable(Symbol noTerm, Symbol term) {
        //Hago un doble recorrido por el set de entradas.
        for (Map.Entry<Symbol, HashMap<Symbol, Production>> entry : table.entrySet()) {
            if (entry.getKey().getName().equals(noTerm.getName())) {
                for (Map.Entry<Symbol, Production> innerEntry : entry.getValue().entrySet()) {
                    if (innerEntry.getKey().getName().equals(term.getName())) {
                        return innerEntry.getValue();
                    }
                }
            }
        }
        return null;
    }
}