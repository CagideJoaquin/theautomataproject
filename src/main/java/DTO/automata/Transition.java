package DTO.automata;

public class Transition {

    private State origen;
    private State destino;
    private String simbolo;

    public Transition(State origen, State destino, String simbolo) {
        this.origen = origen;
        this.destino = destino;
        this.simbolo = simbolo;
    }

    public State getOrigen() {
        return origen;
    }

    public State getDestino() {
        return destino;
    }

    public String getSimbolo() {
        return simbolo;
    }
}
