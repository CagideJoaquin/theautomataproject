package DTO.automata;

import csvReader.AutomataReader;

import java.io.IOException;
import java.util.ArrayList;

public class Automata {
    private ArrayList<State> states;

    private ArrayList<String> alphabet;

    private ArrayList<Transition> transitions;

    //Constructor de automata AFD a partir de los resultados de un AFND
    public Automata(ArrayList<State> states, ArrayList<String> alphabet, ArrayList<Transition> transitions) {
        this.states = states;
        this.alphabet = alphabet;
        this.transitions = transitions;
    }
    //Constructor de automata afnd a partir de un archivo de texto.
    public Automata(String filePath) {
        AutomataReader cs = new AutomataReader();

        try {
            //Obtengo las variables del txt
            this.alphabet = cs.getAlphabet(filePath);
            int numStates = cs.getNumStates(filePath);
            this.states = new ArrayList<>();

            //Creo los estados
            for (int i = 0; i < numStates; i++) {
                this.states.add(new State(String.valueOf(i + 1), "normal"));
            }

            String[] finalStates = cs.getFinalStates(filePath);

            //Seteo cuales son finales
            for (String finalState : finalStates) {
                states.get(Integer.valueOf(finalState.trim()) - 1).setType("final");
            }

            this.transitions = new ArrayList<>();

            ArrayList<String[]> trans = cs.getTransitions(filePath);

            //Creo las transiciones correspondientes
            for (String[] t : trans) {
                String[] var = t[1].split("->");
                transitions.add(new Transition(states.get(Integer.valueOf(t[0].trim()) - 1),
                        states.get(Integer.valueOf(var[1].trim()) - 1),
                        var[0].trim()));
            }

        } catch (IOException e) {
            System.out.println("No se encontró el archivo.");
        }
    }

    public ArrayList<State> getStates() {
        return states;
    }

    public ArrayList<String> getAlphabet() {
        return alphabet;
    }

    public ArrayList<Transition> getTransitions() {
        return transitions;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder("Alfabeto: ");
        for (String s : alphabet) {
            ret.append(s).append(" ");
        }

        ret.append("\n").append("Estados: ");

        for (State s : states) {
            ret.append(s.getName()).append(" ");
        }

        ret.append("\n").append("Estados finales: ");

        for (State s : states) {
            if (s.getType().equals("final")) {
                ret.append(s.getName()).append(" ");
            }
        }

        ret.append("\n").append("Transiciones: ");

        for (Transition t : transitions) {
            ret.append("\n").append("(").append(t.getOrigen().getName()).append(", ").append(t.getSimbolo()).append(") -> ").append(t.getDestino().getName());
        }

        return ret.toString();
    }
}
