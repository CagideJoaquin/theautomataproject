import csvReader.GrammarReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

public class GrammarReaderTest {

    @Test
    public final void productions() {
        GrammarReader r = new GrammarReader();
        ArrayList<String[]> productions = null;
        try {
            productions = r.getProductions("grammar.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertEquals("X_{1}", productions.get(0)[0]);
        Assert.assertEquals("X_{3}", productions.get(0)[2].trim());
        Assert.assertEquals("X_{2}", productions.get(2)[0]);
        Assert.assertEquals("(", productions.get(3)[2].trim());
    }
}