import DTO.automata.Automata;
import DTO.automata.State;
import DTO.automata.Transition;
import main.AutomataModel;
import org.junit.Assert;
import org.junit.Test;

public class AutomataTest {

    //Automata del pdf AFND's. Tablero de movimientos (Sin estados "trampa")
    @Test
    public final void afndToAfdTest1() {
        System.out.println("CORRIENDO TEST\n");
        AutomataModel am = new AutomataModel();
        Automata afnd = new Automata("afnd.csv");
        Automata afd = am.makeAFD(afnd);

        System.out.println(afd.toString());
        Assert.assertEquals(7, afd.getStates().size());
        Assert.assertEquals(afd.getStates().size() * afd.getAlphabet().size(), afd.getTransitions().size());
        System.out.println("\nTEST FINALIZADO\n");
    }

    @Test
    public final void oneTransitionForInputState() {
        AutomataModel am = new AutomataModel();
        Automata afnd = new Automata("afnd.csv");
        Automata afd = am.makeAFD(afnd);

        int cont = 0;

        for (State s : afd.getStates()) {
            for (String alpha : afd.getAlphabet()) {
                for (Transition t : afd.getTransitions()) {
                    if (t.getOrigen().getName().equals(s.getName()) && t.getSimbolo().equals(alpha)) {
                        cont++;
                    }
                }
                Assert.assertEquals(1, cont);
                cont = 0;
            }
        }
    }

    //Automata de la práctica de afnds. Ejercicio 11a. (Con estados "trampa")
    @Test
    public final void afndToAfdTest2() {
        System.out.println("CORRIENDO TEST\n");
        AutomataModel am = new AutomataModel();
        Automata afnd = new Automata("afnd2.csv");
        Automata afd = am.makeAFD(afnd);

        System.out.println(afd.toString());
        Assert.assertEquals(10, afd.getStates().size());
        Assert.assertEquals(afd.getStates().size() * afd.getAlphabet().size(), afd.getTransitions().size());
        System.out.println("\nTEST FINALIZADO\n");
    }

    @Test
    public final void oneTransitionForInputState2() {
        AutomataModel am = new AutomataModel();
        Automata afnd = new Automata("afnd2.csv");
        Automata afd = am.makeAFD(afnd);

        int cont = 0;

        for (State s : afd.getStates()) {
            for (String alpha : afd.getAlphabet()) {
                for (Transition t : afd.getTransitions()) {
                    if (t.getOrigen().getName().equals(s.getName()) && t.getSimbolo().equals(alpha)) {
                        cont++;
                    }
                }
                Assert.assertEquals(1, cont);
                cont = 0;
            }
        }
    }
}
