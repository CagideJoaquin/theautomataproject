import DTO.parser.Grammar;
import DTO.parser.Production;
import DTO.parser.Symbol;
import main.ParserModel;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class GrammarTest {

    @Test
    public final void createGrammar() {
        System.out.println("CORRIENDO TEST\n");
        Grammar grammar = new Grammar("grammar.csv");

        for (Production p : grammar.getProductions()) {
            System.out.println(p.toString());
        }

        Assert.assertEquals(5, grammar.getProductions().size());
        Assert.assertEquals(8, grammar.getGrammarSymbols().size());
        Assert.assertEquals("X_{1}", grammar.getProductions().get(0).getLeft().getName());
        Assert.assertEquals("noTerm", grammar.getProductions().get(0).getLeft().getType());
        Assert.assertEquals("X_{2}", grammar.getProductions().get(1).getLeft().getName());
        Assert.assertEquals("/epsilon", grammar.getProductions().get(2).getRight().get(0).getName());
        System.out.println("\nTEST FINALIZADO\n");
    }

    @Test
    public final void regexTerm() {
        Assert.assertTrue(Pattern.matches("X_\\{\\d+}", "X_{1}"));
        Assert.assertTrue(Pattern.matches("X_\\{\\d+}", "X_{9}"));
        Assert.assertTrue(Pattern.matches("X_\\{\\d+}", "X_{30}"));
        Assert.assertFalse(Pattern.matches("X_\\{\\d+}", "X_{}"));
        Assert.assertFalse(Pattern.matches("X_\\{\\d+}", "X_{1"));
        Assert.assertFalse(Pattern.matches("X_\\{\\d+}", "X{1}"));
    }

    @Test
    public final void getFirst() {
        Grammar g = new Grammar("grammar.csv");
        ParserModel pm = new ParserModel(g);
        Assert.assertEquals(g.getFirst().get(g.getGrammarSymbols().get(0)).get(0).getName(), "(");//Este es x_{1}
        Assert.assertEquals(g.getFirst().get(g.getGrammarSymbols().get(1)).get(0).getName(), "(");//Este es x_{3}
        Assert.assertEquals(g.getFirst().get(g.getGrammarSymbols().get(2)).get(1).getName(), "/epsilon");//Este es x_{2}
        Assert.assertEquals(g.getFirst().size(), 8);
    }

    @Test
    public final void getFollow() {
        Grammar g = new Grammar("grammar.csv");
        ParserModel pm = new ParserModel(g);
        Assert.assertEquals(g.getFollow().get(g.getGrammarSymbols().get(0)).get(0).getName(), "$");//Este es x_{1}
        Assert.assertEquals(g.getFollow().get(g.getGrammarSymbols().get(1)).get(0).getName(), "+");//Este es x_{3}
        Assert.assertEquals(g.getFollow().get(g.getGrammarSymbols().get(2)).get(0).getName(), "$");//Este es x_{2}
        Assert.assertEquals(g.getFollow().size(), 3);
    }

    @Test
    public final void parsingTable() {
        Grammar g = new Grammar("grammar.csv");
        ParserModel pm = new ParserModel(g);

        //Que haya tantas filas como noTerm.
        Assert.assertEquals(3, g.getTable().size());

        //Que hayan tantas entradas como espero:
        int cont = 0;
        for (Map.Entry<Symbol, HashMap<Symbol, Production>> entry : g.getTable().entrySet()) {
            for (Map.Entry<Symbol, Production> innerEntry : entry.getValue().entrySet()) {
                cont++;
            }
        }
        Assert.assertEquals(7, cont);

        //Que las producciones de la tabla sean lo que espero:
        Assert.assertEquals("X_{1}", g.getProdFromTable
                (new Symbol("X_{1}", "noTerm"), new Symbol("(", "term")).getLeft().getName());
        Assert.assertNull(g.getProdFromTable
                (new Symbol("X_{1}", "noTerm"), new Symbol("+", "term")));
    }

    @Test
    public final void isPartOfLanguage() {
        System.out.println("CORRIENDO TEST\n");
        Grammar g = new Grammar("grammar.csv");
        ParserModel pm = new ParserModel(g);
        Symbol s1 = new Symbol("id", "term");
        Symbol s2 = new Symbol("+", "term");
        Symbol s3 = new Symbol("(", "term");
        Symbol s4 = new Symbol("id", "term");
        Symbol s5 = new Symbol("+", "term");
        Symbol s6 = new Symbol("id", "term");
        Symbol s7 = new Symbol(")", "term");
        Symbol s8 = new Symbol("$", "term");

        ArrayList<Symbol> input = new ArrayList<>();
        input.add(s1);
        input.add(s2);
        input.add(s3);
        input.add(s4);
        input.add(s5);
        input.add(s6);
        input.add(s7);
        input.add(s8);

        boolean isPart = pm.isPartOfLanguageG(input);

        Assert.assertTrue(isPart);
        System.out.println(isPart);
        //Le remuevo el "+" entre id y id.
        input.remove(4);

        isPart = pm.isPartOfLanguageG(input);
        Assert.assertFalse(isPart);
        System.out.println(isPart);
        System.out.println("\nTEST FINALIZADO\n");
    }
}