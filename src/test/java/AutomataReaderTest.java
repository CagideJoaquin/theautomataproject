import csvReader.AutomataReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

public class AutomataReaderTest {

    @Test
    public final void getAlphabet() {
        AutomataReader r = new AutomataReader();
        ArrayList<String> alphabet = null;
        try {
            alphabet = r.getAlphabet("afnd.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertEquals("r", alphabet.get(0));
        Assert.assertEquals("b", alphabet.get(1));
    }

    @Test
    public final void numStates() {
        AutomataReader r = new AutomataReader();
        int numStates = 0;
        try {
            numStates = r.getNumStates("afnd.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(9, numStates);
    }

    @Test
    public final void finalStates() {
        AutomataReader r = new AutomataReader();
        String[] finalStates = null;
        try {
            finalStates = r.getFinalStates("afnd.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertEquals("9", finalStates[0].trim());
    }

    @Test
    public final void transitions() {
        AutomataReader r = new AutomataReader();
        ArrayList<String[]> transitions = null;
        try {
            transitions = r.getTransitions("afnd.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertEquals("1", transitions.get(0)[0]);
        Assert.assertEquals("r -> 2", transitions.get(0)[1].trim());
        Assert.assertEquals("1", transitions.get(2)[0]);
        Assert.assertEquals("r -> 4", transitions.get(3)[1].trim());
    }
}